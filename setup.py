import setuptools 
with open("README.md", "r") as fh:
    long_description = fh.read() 
    setuptools.setup(
    name="avatrader",
    version="0.0.1",
    author="AVA alpha",
    author_email="krittipong@ava.fund",
    description="avatrader",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://github.com/pypa/sampleproject",
    packages=['avatrader'],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
