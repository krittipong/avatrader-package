import os
import numpy
import pandas

from .orders import Orders

class Portfolio(object):

    def __init__(self, environment):
        self._environment      = environment
        self._position_columns = ['SID', 'SYMBOL', 'VOLUME', 'COST']
        self._equity_columns   = ['DATE', 'CASH', 'EQUITY', 'HOLIDAY']

        self.positions         = pandas.DataFrame(columns=self._position_columns)
        self.positions.set_index('SID', drop=False, inplace=True)

        self.equity            = pandas.Series(numpy.nan, index=self._equity_columns)
        self.equity['DATE']    = environment.current_date()
        
        self._environment.logger.info("Portfolio.__init__(): done")

    def from_date(self, trading_date):
        
        # 1. load actual positions
        assert os.path.exists(trading_date.actual_positions)
        self.positions      = pandas.read_csv(
            filepath_or_buffer = trading_date.actual_positions, 
            skipinitialspace   = True, 
            index_col          = False)
        self.positions.set_index('SID', drop=False, inplace=True)
        
        assert numpy.array_equal(self.positions.columns, self._position_columns)
        
        
        # 2. load actual equaity
        assert os.path.exists(trading_date.actual_equity)
        self.equity         = pandas.read_csv(
            filepath_or_buffer = trading_date.actual_equity, 
            skipinitialspace   = True, 
            index_col          = 0, 
            squeeze            = True, 
            header             = None)
        self.equity['DATE']   = pandas.to_datetime(self.equity['DATE']).date()
        self.equity['CASH']   = pandas.to_numeric(self.equity['CASH'], errors='coerse')
        self.equity['EQUITY'] = pandas.to_numeric(self.equity['EQUITY'], errors='coerse')
        
        assert numpy.array_equal(self.equity.index, self._equity_columns)
        print(self.equity['DATE'])
        print(trading_date())
        assert self.equity['DATE']   == trading_date()
        assert self.equity['CASH']   >= 0
        assert self.equity['EQUITY'] >= 0

        # 3. load custom fields from previous target positions
        assert os.path.exists(trading_date.target_positions)
        target_positions = pandas.read_csv(
            filepath_or_buffer = trading_date.target_positions, 
            skipinitialspace   = True, 
            index_col          = False)
        target_positions.set_index('SID', drop=False, inplace=True)
        target_positions.drop(self._position_columns, axis='columns', inplace=True)
        self.positions = self.positions.join(target_positions, how='outer').fillna(0)
        
        # 4. logging
        self._environment.logger.debug(
            "\n\nPortfolio.from_date({:%Y-%m-%d}): positions\n{}\n".format(trading_date(), self.positions))
        self._environment.logger.debug(
            "\n\nPortfolio.from_date({:%Y-%m-%d}): equity\n{}\n".format(trading_date(), self.equity))
        self._environment.logger.info(
            "\n\nPortfolio.from_date({:%Y-%m-%d}): done".format(trading_date()))

        return self

    def save_as_target_portfolio(self):
        # 1. save target positions only to current date
        assert numpy.array_equal(self.positions[self._position_columns].columns, self._position_columns)
        assert not os.path.exists(self._environment.current_date.target_positions)
        
        self.positions.to_csv(self._environment.current_date.target_positions, index=False)
        
        # 2. save target equity only to current date
        assert self.equity['DATE']   == self._environment.current_date()
        assert self.equity['CASH']   >= 0 or pandas.isnull(self.equity['CASH'])
        assert self.equity['EQUITY'] >= 0 or pandas.isnull(self.equity['EQUITY'])
        assert numpy.array_equal(self.equity.index, self._equity_columns)
        assert not os.path.exists(self._environment.current_date.target_equity)
        
        self.equity.to_csv(self._environment.current_date.target_equity, header=False)
              
        # 4. logging
        self._environment.logger.debug(
            "\n\nPortfolio.save_as_target_portfolio(): positions\n{}\n".format(self.positions))
        self._environment.logger.debug(
            "\n\nPortfolio.save_as_target_portfolio(): equity\n{}\n".format(self.equity))
        self._environment.logger.info(
            "\n\nPortfolio.save_as_target_portfolio(): save to {:%Y-%m-%d}, done".format(
            self._environment.current_date()))


    def save_as_actual_portfolio(self):

        # 1. save actual positions only to previous date
        # assert numpy.array_equal(self.positions.columns, self._position_columns)
        assert not os.path.exists(self._environment.previous_date.actual_positions)
        #print('hello')
        #print(self.positions)
        self.positions = self.positions[self.positions['VOLUME']!=0]
        assert 0.0 not in self.positions['VOLUME'].values
        
        
        self.positions[self._position_columns].to_csv(self._environment.previous_date.actual_positions, index=False)
        
        # 2. save actual equity only to previous date
        assert self.equity['DATE']   == self._environment.previous_date()
        assert self.equity['CASH']   >= 0
        assert self.equity['EQUITY'] >= 0
        assert numpy.array_equal(self.equity.index, self._equity_columns)
        assert not os.path.exists(self._environment.previous_date.actual_equity)

        self.equity.to_csv(self._environment.previous_date.actual_equity, header=False)

        # 3. logging
        self._environment.logger.debug(
            "\n\nPortfolio.save_as_actual_portfolio(): positions\n{}\n".format(self.positions))
        self._environment.logger.debug(
            "\n\nPortfolio.save_as_actual_portfolio(): equity\n{}\n".format(self.equity))
        self._environment.logger.info(
            "\n\nPortfolio.save_as_actual_portfolio(): save to {:%Y-%m-%d}, done".format(
            self._environment.previous_date()))

    def __sub__(self, portfolio):
        self._environment.logger.info(
            "\n\nPortfolio.__sub__(portfolio from {:%Y-%m-%d}): called".format(portfolio.equity['DATE']))
        assert isinstance(portfolio, Portfolio)
        return Orders(self._environment).from_portfolios(self, portfolio)
