import requests
import pandas
import numpy
import json
from .portfolio import Portfolio

class SkynetAPI(object):
    def __init__(self, environment, market_data):
        self._environment = environment
        self._market_data = market_data
        
    def load_portfolio(self):
        url = f'https://api.skynetsystems.co.th/api/v1/account/900/{self._environment.skynet.accountno}'
        headers = {'X-API-KEY': self._environment.skynet.key}
        response = requests.get(url, headers=headers)

        account_info = response.json()
        assert account_info['status']=='T', 'error loading portfolio {}'.format(account_info)

        url = f'https://api.skynetsystems.co.th/api/v1/portfolio/900/{self._environment.skynet.accountno}'
        headers = {'X-API-KEY': self._environment.skynet.key}
        response = requests.get(url, headers=headers)
        portfolio_info = response.json()
        assert portfolio_info['status']=='T', 'error loading portfolio {}'.format(portfolio_info)
        default_position_column = ['SID', 'SYMBOL', 'VOLUME', 'COST']

        actual_portfolio = Portfolio(self._environment)
        actual_portfolio.equity['CASH'] = account_info['data']['account']['line']
        actual_portfolio.equity['DATE'] = self._environment.current_date()
        actual_portfolio.equity['EQUITY'] = account_info['data']['account']['equity']
        actual_portfolio.equity['HOLIDAY'] = '' # TODO: EDIT THIS
        for position in portfolio_info['data']['portfolios']:
            actual_position = pandas.Series({
                'SID': self._market_data.universes[self._market_data.universes['SYMBOL']==position['symbol']]['SID'].values[0],
                'SYMBOL': position['symbol'],
                'VOLUME': position['actVol'],
                'COST': position['prcAmtVal']
            })
            actual_portfolio.positions = actual_portfolio.positions.append(actual_position, ignore_index=True)
        actual_portfolio.positions['SID'] = actual_portfolio.positions['SID'].astype(numpy.int64)
        actual_portfolio.positions.set_index('SID', drop=False, inplace=True)
        return actual_portfolio
        

    def submit_order(self):
        orders = pandas.read_csv(self._environment.current_date.target_orders)
        for index, order in orders.iterrows():          
            url = self._environment.skynet.api.placeorder
            headers = {'X-API-KEY': self._environment.skynet.key,
                      'Content-Type': 'application/json'}
            symbol = order['SYMBOL']
            side = 'B' if order['ACTION'] == 'BUY' else 'S'
            volume = order['VOLUME']
            price = order['LIMITPRICE']
            accountNo = self._environment.skynet.accountno
            payload = {  
               'accountNo': accountNo,
               'symbol': symbol,
               'position':"O",
               'side': side,# 'B' or 'S'
               'volume': volume,
               'price': price,
               'priceType': 'LIMIT',
               'validityType': 'DAY',
               'takeProfitPrice': 0,
               'stopLossPrice': 0,
               'note': '',
               'pbVolume': '',
               'ttf': 'NORMAL'
            }
            print(payload)
            response = requests.post(url, headers=headers, data=json.dumps(payload))
            assert response.json()['status']=='T', 'Ordersend ERROR!!, {}'.format(response.json())