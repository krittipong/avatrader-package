import pandas
import os
import pyfolio
from avatrader import environment

class Report(object):
    def __init__(self, environment):
        self._environment = environment
        self._equity_default_columns = ['DATE', 'CASH', 'EQUITY', 'HOLIDAY'] # TODO: should read from portfolio class?
        self._benchmark = 1024
        self.port_dir = '{}portfolios'.format(environment.root_dir)
        self.equity = pandas.DataFrame(columns=self._equity_default_columns)
        
    def generate_report(self, market_data, start, end):
        # 1. generate csv file: read all equity files
        for p in os.walk(self.port_dir):
            path = p[0] 
            actual_equity_path = '{}/actual_equity.csv'.format(path)
            if os.path.isfile(actual_equity_path):
                equity_row = pandas.read_csv(filepath_or_buffer = actual_equity_path,
                                             skipinitialspace   = True,
                                             index_col          = 0,
                                             squeeze            = True,
                                             header             = None,
                                             engine             = 'python')

                equity_row['DATE']   = pandas.to_datetime(equity_row['DATE'])
                equity_row['CASH']   = pandas.to_numeric(equity_row['CASH'], errors='coerse')
                equity_row['EQUITY'] = pandas.to_numeric(equity_row['EQUITY'], errors='coerse')
                self.equity = self.equity.append(equity_row)
        
        self.equity = self.equity.set_index('DATE').sort_index()
        self.equity = self.equity[start:end]
        print(self.equity.head())
        
        # 2. prepare data for pyfolio
        self.equity['PORTFOLIO_PCT_CHANGE'] = self.equity['EQUITY'].pct_change()
        benchmark_equity = market_data.securities[self._benchmark][['CLOSE']].rename(columns={'CLOSE': 'BENCHMARK_INDEX'})
        benchmark_equity = benchmark_equity[start:end]
        benchmark_equity['BENCHMARK_PCT_CHANGE'] = benchmark_equity['BENCHMARK_INDEX'].pct_change()
        
        print(self.equity.head())
        print(benchmark_equity.head())
        
        self.equity = self.equity.join(benchmark_equity,how='inner')
        self.equity = self.equity.fillna(0)
        
        self.equity['PORTFOLIO_PCT_CHANGE'] = self.equity['PORTFOLIO_PCT_CHANGE'].astype(float)
        self.equity['BENCHMARK_PCT_CHANGE'] = self.equity['BENCHMARK_PCT_CHANGE'].astype(float)
        
        self.equity.to_csv('{}portfolios/equity.csv'.format(self._environment.root_dir))
        # 3. generate png files: using generated file to generate png report files
        
        self.equity = pandas.read_csv('{}portfolios/equity.csv'.format(self._environment.root_dir),
                                      index_col=0, parse_dates=True, header=None)
        
        simple_tearsheet_path = '{}portfolios/simple_tearsheet.png'.format(self._environment.root_dir)
        
        port_nav       = self.equity[4][1:]
        port_nav       = port_nav.convert_objects(convert_numeric=True)
        port_nav.index = pandas.to_datetime(port_nav.index)

        benchmark_nav       = self.equity[6][1:]
        benchmark_nav       = benchmark_nav.convert_objects(convert_numeric=True)
        benchmark_nav.index = pandas.to_datetime(benchmark_nav.index)

        port_nav.index       = port_nav.index.tz_localize("UTC")
        benchmark_nav.index  = benchmark_nav.index.tz_localize("UTC")

        simple_tearsheet = pyfolio.create_returns_tear_sheet(port_nav, benchmark_rets=benchmark_nav, return_fig=True)
        simple_tearsheet.savefig(simple_tearsheet_path)
        
    def save_as_csv(self):
        self.equity.to_csv(self._environment.report.equity)
        
    def save_as_png(self):
        self.tearsheet.savefig(self._environment.report.tearsheet)