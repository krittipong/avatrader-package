import numpy
import pandas

from .portfolio import Portfolio
from .orders import Orders

class Broker(object):
    def __init__(self, environment, slippage, commissions, volume_limit):
        self._environment  = environment
        self._slippage     = slippage
        self._commissions  = commissions
        self._volume_limit = volume_limit
        self._index_sid    = 1024
        self._environment.logger.info("Broker.__init__(): done")

    def create_initial_portfolio(self, initial_cash):
        new_actual_portfolio = Portfolio(self._environment)
        new_actual_portfolio.equity['DATE']   = self._environment.previous_date()
        new_actual_portfolio.equity['CASH']   = initial_cash
        new_actual_portfolio.equity['EQUITY'] = initial_cash
        new_actual_portfolio.save_as_actual_portfolio()

        new_target_positions = pandas.DataFrame(columns=new_actual_portfolio.positions.columns)
        new_target_positions.to_csv(self._environment.previous_date.target_positions, index=False)

        new_target_equity = pandas.Series(index=new_actual_portfolio.equity.index)
        new_target_equity.to_csv(self._environment.previous_date.target_equity, header=False)
        
        self._environment.logger.info(
            "Broker.create_initial_portfolio(cash={}): save to {:%Y-%m-%d}, done".format(
                initial_cash,
                self._environment.previous_date()))

    def generate_actual_portfolio(self, market_data):
        
        ######################################
        # 0. check if holiday or trading day #
        ######################################
        last_trading_date = market_data.securities[self._index_sid]['DATE'].iat[-1].date()
        
        if self._environment.previous_date() != last_trading_date:
            current_portfolio = Portfolio(self._environment).from_date(self._environment.before_previous_date)
            actual_portfolio  = Portfolio(self._environment)
            actual_portfolio.positions = current_portfolio.positions
            actual_portfolio.equity    = current_portfolio.equity
            actual_portfolio.equity['DATE']    = self._environment.previous_date()
            actual_portfolio.equity['HOLIDAY'] = True
            return actual_portfolio

        current_portfolio = Portfolio(self._environment).from_date(self._environment.before_previous_date)
        target_orders     = Orders(self._environment).from_date(self._environment.previous_date)
        print(target_orders)
        target_orders     = target_orders if target_orders.empty else self._adjust_price_and_volume(target_orders, market_data) 
        print('after')
        print(target_orders)
        
        #################################
        # 1. update positions dataframe #
        #################################
        
        # 1.1 join position tables
        positions_diff = current_portfolio.positions.join(
            other   = target_orders,
            how     = 'outer',
            lsuffix = '_CURRENT',
            rsuffix = '_DIFF')
        
        # 1.2 position to hold
        positions_to_hold = positions_diff[positions_diff['ACTION'].isnull()].copy()
        positions_to_hold['SID']    = positions_to_hold['SID_CURRENT']
        positions_to_hold['SYMBOL'] = positions_to_hold['SYMBOL_CURRENT']
        positions_to_hold['VOLUME'] = positions_to_hold['VOLUME_CURRENT']
        positions_to_hold['COST']   = positions_to_hold['COST']
        positions_to_hold           = positions_to_hold[positions_to_hold['VOLUME'] > 0]
        
        # 1.3 handle sell orders
        positions_to_sell = positions_diff[positions_diff['ACTION'] == 'SELL'].copy()
        positions_to_sell['SID']    = positions_to_sell['SID_DIFF']
        positions_to_sell['SYMBOL'] = positions_to_sell['SYMBOL_DIFF']
        positions_to_sell['VOLUME'] = positions_to_sell['VOLUME_CURRENT'] - positions_to_sell['VOLUME_DIFF']
        positions_to_sell['COST']  *= 1. - positions_to_sell['VOLUME_DIFF'] / positions_to_sell['VOLUME_CURRENT']

        # 1.4 handle buy orders
        positions_to_buy = positions_diff[positions_diff['ACTION'] == 'BUY'].fillna(0).copy()
        positions_to_buy['SID']    = positions_to_buy['SID_DIFF']
        positions_to_buy['SYMBOL'] = positions_to_buy['SYMBOL_DIFF']
        positions_to_buy['VOLUME'] = positions_to_buy['VOLUME_CURRENT'] + positions_to_buy['VOLUME_DIFF']
        positions_to_buy['COST']  += (
            + positions_to_buy['VOLUME_DIFF'] 
            * positions_to_buy['LIMITPRICE']
            * (1. + self._commissions) )
        
        # 1.5 calculate cash spent
        positions_to_buy['CASH_DIFF']   = (
            + positions_to_buy['VOLUME_DIFF'] 
            * positions_to_buy['LIMITPRICE']
            * (1. + self._commissions))
        
        positions_to_sell['CASH_DIFF']  = (
            + positions_to_sell['VOLUME_DIFF'] 
            * positions_to_sell['LIMITPRICE']
            * (1. - self._commissions))
        
        

        ##############################
        # 2. update equity dataframe #
        ##############################
        # 2.1 construct actual portfolio
        actual_portfolio = Portfolio(self._environment)
        actual_portfolio.positions = positions_to_hold
        actual_portfolio.positions = actual_portfolio.positions.append(positions_to_sell, sort=False)
        actual_portfolio.positions = actual_portfolio.positions.append(positions_to_buy, sort=False)
        actual_portfolio.positions = actual_portfolio.positions[current_portfolio.positions.columns]
        actual_portfolio.positions = actual_portfolio.positions[actual_portfolio.positions['VOLUME'] > 0]
        actual_portfolio.positions['SID'] = actual_portfolio.positions['SID'].astype(numpy.int64)
        actual_portfolio.positions.set_index('SID', drop=False, inplace=True)
        assert numpy.array_equal(actual_portfolio.positions.columns, current_portfolio.positions.columns)

        # 2.2 update cash
        actual_portfolio.equity['CASH'] = (
                + current_portfolio.equity['CASH']
                - positions_to_buy['CASH_DIFF'].sum()
                + positions_to_sell['CASH_DIFF'].sum())
        
        assert (positions_to_buy['CASH_DIFF'] >= 0).all()
        assert (positions_to_sell['CASH_DIFF'] >= 0).all()
        assert actual_portfolio.equity['CASH'] >= 0

        # 2.3 update equity
        actual_market_values = actual_portfolio.positions.apply(
            func        = lambda record: record['VOLUME'] * market_data.securities[record['SID']]['CLOSE'].iat[-1], 
            axis        = 1, 
            result_type = 'reduce')
        
        actual_portfolio.equity['DATE']    = self._environment.previous_date()
        actual_portfolio.equity['EQUITY']  = actual_portfolio.equity['CASH'] + actual_market_values.sum()
        actual_portfolio.equity['HOLIDAY'] = False

        # 3. logging
        self._environment.logger.debug(
            "Broker.generate_actual_portfolio(market_data): created for {:%Y-%m-%d}, positions.head()\n{}".format(
                actual_portfolio.equity['DATE'],
                actual_portfolio.positions.head()))
        self._environment.logger.debug(
            "Broker.generate_actual_portfolio(market_data): created for {:%Y-%m-%d}, equity.head()\n{}".format(
                actual_portfolio.equity['DATE'],
                actual_portfolio.equity.head()))
        self._environment.logger.info(
            "Broker.generate_actual_portfolio(market_data): created for {:%Y-%m-%d}, done".format(
                self._environment.previous_date()))
            
        return actual_portfolio

    def _adjust_price_and_volume(self, target_orders, market_data):
        
        # 0. retrieve market volume and price
        target_orders[['MARKET_VOLUME', 'MARKET_PRICE']] = target_orders.apply(
            func        = lambda record: market_data.securities[record['SID']][['VOLUME', 'OPEN']].iloc[-1],
            axis        = 1, 
            result_type = 'reduce')
            
            
        # 1. limit volume to trade
        target_orders['MARKET_VOLUME'] = self._volume_limit * target_orders['MARKET_VOLUME']
        target_orders['MARKET_VOLUME'] = target_orders[['VOLUME', 'MARKET_VOLUME']].min(axis='columns')
        target_orders['MARKET_VOLUME'] = numpy.floor(target_orders['MARKET_VOLUME'] / 100.) * 100.
        target_orders['VOLUME']        = target_orders['MARKET_VOLUME']

        # 2. limit price to trade     
        to_buy  = target_orders['ACTION'] == 'BUY'
        to_sell = target_orders['ACTION'] == 'SELL'
        target_orders.loc[to_buy,  'MARKET_PRICE'] *= (1. + self._slippage)
        target_orders.loc[to_sell, 'MARKET_PRICE'] *= (1. - self._slippage)

        is_overpriced  = to_buy  & (target_orders['MARKET_PRICE'] > target_orders['LIMITPRICE'])
        is_underpriced = to_sell & (target_orders['MARKET_PRICE'] < target_orders['LIMITPRICE'])
        target_orders  = target_orders[~is_overpriced & ~is_underpriced].copy()
        target_orders['LIMITPRICE'] = target_orders['MARKET_PRICE']

        # 3. drop auxilary columns
        target_orders.drop(['MARKET_PRICE', 'MARKET_VOLUME'], axis='columns', inplace=True)
            
        # 4. logging
        self._environment.logger.debug("Broker._adjust_price_and_volume(orders, market_data): done")
            
        return target_orders
