import pandas
import talib
import numpy

class Security(pandas.DataFrame):

    def __init__(self, environment, sid):

        query_string = """
    SELECT
        ohlcvv_day.id       AS SID,
        ohlcvv_day.date_of  AS DATE,
        ohlcvv_day.open     AS OPEN, 
        ohlcvv_day.high     AS HIGH, 
        ohlcvv_day.low      AS LOW, 
        ohlcvv_day.close    AS CLOSE, 
        ohlcvv_day.vol      AS VOLUME,
        ohlcvv_day.val      AS VALUE,
        nvdr.val_buy        AS NVDR_BUY,
        nvdr.val_sell       AS NVDR_SELL,
        SQ."SQDebtRatio" AS DA,
        SQ."ROE" AS ROE,
        SQ."ROA" AS ROA,
        SQ."NPM" AS NPM,
        SQ."SQNP" AS NET_PROFIT,
        SQ."PE" AS PE,
        SQ."PBV" AS PBV
    FROM ohlcvv_day
    LEFT JOIN nvdr

        ON  nvdr.id = ohlcvv_day.id
        AND nvdr.date_of = ohlcvv_day.date_of
    
    LEFT JOIN (SELECT FUN.*, SC.id FROM siamquant_fundamental AS FUN
                        INNER JOIN  (SELECT * FROM securities) SC
                        ON FUN."Ticker" = SC.symbol
         ) SQ
        ON SQ.id = ohlcvv_day.id
        AND SQ."Date/Time" = ohlcvv_day.date_of
        
    WHERE ohlcvv_day.id = {} AND ohlcvv_day.date_of < {:%Y%m%d}
    ORDER BY DATE ASC

""".format(sid, environment.current_date())

        super().__init__(environment.database.query(query_string))
        
        self.columns = self.columns.str.upper()
        self['DATE'] = pandas.to_datetime(self['DATE'], format='%Y%m%d')
        self.set_index('DATE', drop=False, inplace=True)
        
        environment.logger.info("Security.__init__({}): done".format(sid))
        

    def __getitem__(self, names):
        names_to_check = names if isinstance(names, list) else [names]
        for name in names_to_check:
            if name not in self.columns:
                self[name] = self._init_column(name)
        return super().__getitem__(names)


    def _init_column(self, func_params):
        func_params = func_params if isinstance(func_params, tuple) else (func_params,)

        func = getattr(self, func_params[0])
        params = func_params[1:]
        return func(*params)

    ##############
    # indicators #
    ##############

    def SMA(self, n):
        return self['CLOSE'].rolling(window=n).mean()
    
    def TENKAN(self):
        nine_period_high = self['HIGH'].rolling(window= 9).max()
        nine_period_low = self['LOW'].rolling(window= 9).min()
        return (nine_period_high + nine_period_low) /2
    
    def KIJUN(self):
        period26_high =self['HIGH'].rolling(window= 26).max()
        period26_low = self['LOW'].rolling(window= 26).min()
        return (period26_high + period26_low) / 2
    
    def EMA(self, n):
        return talib.EMA(numpy.array(self['CLOSE'].values,dtype='f8'), n)

    def ROC(self, n):
        return talib.ROC(numpy.array(self['CLOSE'].values,dtype='f8'), n)
    
    def HV(self, n):
        ret = numpy.log(self['CLOSE'] / self['CLOSE'].shift(1))
        ret = ret.rolling(window=n, center=False).std() * numpy.sqrt(n)
        return ret
    
    def SECTORWEIGHT(self):
        print(self.columns)
        sector_weight = pandas.read_csv('porprotions.csv')
        sector_weight['date'] = pandas.to_datetime(sector_weight['date'])
        sector_weight = sector_weight.set_index('date')[:'2012-01-06'].iloc[-1].to_dict()
        
        secname = self.universes.loc[self['SID'].values[0]].drop(['SET','SET100'])
        
        secname = secname.index[secname==True][0]
        return [secname]*len(self)

    def spread(self):
        def get_spread(price):
            if price < 2:
                spread = 0.01
            elif price < 5:
                spread = 0.02
            elif price < 10:
                spread = 0.05
            elif price < 25:
                spread = 0.10
            elif price < 100:
                spread = 0.25
            elif price < 200:
                spread = 0.5
            elif price < 400:
                spread = 1.0
            else:
                spread = 2.0
            return spread
        return self['CLOSE'].apply(get_spread)
         

class Universe(pandas.DataFrame):

    def __init__(self, environment):
        
        query_string = """
            SELECT metadata.* 
            FROM metadata
            
            INNER JOIN 
                (
                    SELECT id, MAX(datetime) as datetime 
                    FROM metadata
                    WHERE metadata.datetime < '{:%Y-%m-%d}'
                    GROUP BY id
                ) AS filtered_metadata
            
                ON  metadata.datetime = filtered_metadata.datetime 
                AND metadata.id       = filtered_metadata.id
                
        """.format(environment.current_date())
            
        super().__init__(environment.database.query(query_string))
        self.columns = self.columns.str.upper()
        self.rename(columns={'ID': 'SID'}, inplace=True)
        self.drop('DATETIME', axis='columns', inplace=True)
        self.set_index('SID', drop=False, inplace=True)

        self.__dict__['_security_columns'] = ['SID', 'SYMBOL']
        self.__dict__['indices_columns']   = ['SET100', 'SET50', 'SET']
        self.__dict__['sectors_columns']   = [
            'AGRI', 'AUTO', 'BANK', 'COMM', 'CONMAT', 'CONS', 'ENERG', 
            'ETRON', 'FASHION', 'FIN', 'FOOD', 'HELTH', 'HOME', 'ICT', 
            'IMM', 'INSUR', 'MEDIA', 'MINE', 'PAPER', 'PERSON', 'PETRO', 
            'PF&REIT', 'PKG', 'PROF', 'PROP', 'STEEL', 'TOURISM', 'TRANS']
        
        environment.logger.info("Universe.__init__(): done")


class MarketData(object):
    class lazydict(dict):
        def __init__(self, environment):
            self._environment = environment

        def __getitem__(self, sid):
            if not sid in self.__dict__:
                self.__dict__[sid] = Security(self._environment, sid)
            return self.__dict__[sid]

    def __init__(self, environment):
        self.universes  = Universe(environment)
        self.securities = MarketData.lazydict(environment)
        environment.logger.info("MarketData.__init__(): done")
        
####################################################################################################

class LocalSecurity(pandas.DataFrame):
    filepath = 'cache.pickle'
    def __init__(self, environment, sid, cache_df):

        df = cache_df[(cache_df['SID']==sid) & (cache_df['DATE'] <environment.current_date()) ]
        super().__init__(df)
        
        self.columns = self.columns.str.upper()
        self['DATE'] = pandas.to_datetime(self['DATE'], format='%Y%m%d')
        self.set_index('DATE', drop=False, inplace=True)
        
        environment.logger.info("Security.__init__({}): done".format(sid))
        
    def __getitem__(self, names):
        names_to_check = names if isinstance(names, list) else [names]
        for name in names_to_check:
            if name not in self.columns:
                self[name] = self._init_column(name)
        return super().__getitem__(names)

class LocalMarketData(object):
    filepath = 'cache.pickle'

    def __init__(self, environment):
        self.universes  = Universe(environment)
        # self.securities = LocalMarketData.lazydict(environment)
        filtered_df = pandas.read_pickle(self.filepath).set_index('DATE', drop=False)
        filtered_df = filtered_df[filtered_df.index < environment.current_date()]
        self.securities = dict(tuple(filtered_df.groupby('SID')))
        environment.logger.info("MarketData.__init__(): done")
        
        
