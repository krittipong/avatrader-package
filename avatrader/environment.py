import os
import sys
import numpy
import pandas
import psycopg2
import logging

class Environment(object):

    def __init__(self, root_dir, current_date):
        self.root_dir             = root_dir
        self.current_date         = self._init_trading_date(current_date)
        self.previous_date        = self._init_trading_date(current_date - pandas.Timedelta(days=1))
        self.before_previous_date = self._init_trading_date(current_date - pandas.Timedelta(days=2))

        self.database = self._init_database()
        self.skynet = self._init_skynet()
        self.logger   = self._init_logger(current_date)
        self.logger.info("Environment.__init__({:%Y-%m-%d}): done".format(current_date))

    def _init_trading_date(self, trading_date):
        relpath_to_actual_positions = "portfolios/{:%Y/%m/%d}/actual_positions.csv"
        relpath_to_actual_equity    = "portfolios/{:%Y/%m/%d}/actual_equity.csv"
        relpath_to_target_positions = "portfolios/{:%Y/%m/%d}/target_positions.csv"
        relpath_to_target_orders    = "portfolios/{:%Y/%m/%d}/target_orders.csv"
        relpath_to_target_equity    = "portfolios/{:%Y/%m/%d}/target_equity.csv"

        # 1. use environment.trading_date() to get the datetime object
        trading_path = lambda: pandas.to_datetime(trading_date).date()
        trading_path.actual_positions = os.path.join(self.root_dir, relpath_to_actual_positions.format(trading_date))
        trading_path.actual_equity    = os.path.join(self.root_dir, relpath_to_actual_equity.format(trading_date))
        trading_path.target_positions = os.path.join(self.root_dir, relpath_to_target_positions.format(trading_date))
        trading_path.target_orders    = os.path.join(self.root_dir, relpath_to_target_orders.format(trading_date))
        trading_path.target_equity    = os.path.join(self.root_dir, relpath_to_target_equity.format(trading_date))

        # 2. environment object is fully responsible for creating and maintaining directories
        os.makedirs(os.path.dirname(trading_path.actual_positions), exist_ok=True)
        os.makedirs(os.path.dirname(trading_path.actual_equity), exist_ok=True)
        os.makedirs(os.path.dirname(trading_path.target_positions), exist_ok=True)
        os.makedirs(os.path.dirname(trading_path.target_orders), exist_ok=True)

        return trading_path

    def _init_database(self):
        database            = lambda: None
        database.host       = "localhost"
        database.name       = "securities_master"
        database.username   = "securities_master"
        database.password   = "inter pret5"
        database.connection = psycopg2.connect(
            host     = database.host,
            database = database.name,
            user     = database.username,
            password = database.password)
        database.query = lambda query_string: pandas.read_sql(sql=query_string, con=database.connection)

        return database

    def _init_logger(self, trading_date):
        logfile = "logs/{:%Y-%m-%d}.txt"
        logfile = os.path.join(self.root_dir, logfile.format(trading_date))
        os.makedirs(os.path.dirname(logfile), exist_ok=True)

        logging.basicConfig(
            filename=logfile,
            level=logging.DEBUG,
            format='%(asctime)s process_id:%(process)d: %(message)s')
        
        if len(logging.getLogger().handlers) == 1:
            logging.getLogger().addHandler(logging.StreamHandler(sys.stdout))
        
        """
        How to use logger:
        1. environment.logger.debug("   log all information for debugging                      ")
        2. environment.logger.info("    log the current state of the running platform          ")
        3. environment.logger.warning(" log unexpected inputs, trying to handle properly       ")
        4. environment.logger.error("   log unexpected inputs, uncapleble of handling properly ")
        """

        return logging.getLogger()
    
    def _init_skynet(self):
        skynet = lambda: None
        skynet.accountno = '1027693-1'
        skynet.key = '7A6F3F04026CE12AC6180D0F135B3983'
        skynet.brokerCode = '900'
        
        skynet.api = lambda: None
        api_placeorder_url = 'https://api.skynetsystems.co.th/api/v1/placeOrder/{brokerCode}'
        
        skynet.api.placeorder = api_placeorder_url.format(brokerCode=skynet.brokerCode)
        return skynet
