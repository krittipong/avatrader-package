import pytest
import sys
import shutil
sys.path.append('/home/yort') 
from avatrader import Broker, Environment
import os
import pandas

@pytest.fixture
def setup_init_():
    print('SETUP INIT')
    test_folder = 'test_folder'
    current_date = pandas.datetime.today()
    if not os.path.exists(test_folder):
        os.makedirs(test_folder)
    environment = Environment(test_folder, current_date)
    yield environment
    print('TEARDOWN INIT')


def test_Broker_init_():
    ## SETUP ##
    test_folder = 'test_Broker_init_'
    assert not os.path.exists(test_folder)
    if not os.path.exists(test_folder):
        os.makedirs(test_folder)    
    ##
    slippage = 0.0025
    commissions = 0.0016
    volume_limit = 0.05
    current_date = pandas.datetime.today()
    environment = Environment(test_folder, current_date)
    broker = Broker(environment, slippage, commissions, volume_limit)
    assert isinstance(broker, Broker)
    assert broker._index_sid == 1024
    ## TEAR DOWN ##
    shutil.rmtree(test_folder) 
    ##
    
def test_broker_create_initial_portfolio():
    ## SETUP ##
    test_folder = 'test_broker_create_initial_portfolio'
    assert not os.path.exists(test_folder)
    if not os.path.exists(test_folder):
        os.makedirs(test_folder)    
    slippage = 0.0025
    commissions = 0.0016
    volume_limit = 0.05
    current_date = pandas.datetime.today()
    environment = Environment(test_folder, current_date)
    broker = Broker(environment, slippage, commissions, volume_limit)
    ##
    
    relpath_to_actual_positions = "portfolios/{:%Y/%m/%d}/actual_positions.csv"
    relpath_to_actual_equity    = "portfolios/{:%Y/%m/%d}/actual_equity.csv"
    relpath_to_target_positions = "portfolios/{:%Y/%m/%d}/target_positions.csv"
    relpath_to_target_orders    = "portfolios/{:%Y/%m/%d}/target_orders.csv"
    relpath_to_target_equity    = "portfolios/{:%Y/%m/%d}/target_equity.csv"
        
    initial_cash = 500000000
    broker.create_initial_portfolio(initial_cash)
    # TEST EQUITY FILE
    equity         = pandas.read_csv(
            filepath_or_buffer = os.path.join(test_folder, relpath_to_actual_equity.format(current_date - pandas.DateOffset(days=1))), 
            skipinitialspace   = True, 
            index_col          = 0, 
            squeeze            = True, 
            header             = None)
    equity['DATE']   = pandas.to_datetime(equity['DATE'])
    equity['CASH']   = pandas.to_numeric(equity['CASH'], errors='coerse')
    equity['EQUITY'] = pandas.to_numeric(equity['EQUITY'], errors='coerse')
    assert equity['DATE'] == pandas.datetime.today().date()
    assert equity['CASH'] == initial_cash
    assert equity['EQUITY'] == initial_cash
    
    # TEST TARGET POSITIONS FILE
    assert os.path.exists(os.path.join(test_folder, relpath_to_target_positions.format(current_date - pandas.DateOffset(days=1))))
    target_positions = pandas.read_csv(
        filepath_or_buffer = os.path.join(test_folder, relpath_to_target_positions.format(current_date - pandas.DateOffset(days=1))), 
        skipinitialspace   = True, 
        index_col          = False)
    assert all(x in ['SID', 'SYMBOL', 'VOLUME', 'COST', 'TARGET_COST', 'TARGET_VOLUME'] for x in target_positions.columns.values)
    
    
    
    
    ## TODO TEST
    
    ## TEAR DOWN ##
    shutil.rmtree(test_folder) 
    ##
    
def test_generate_actual_portfolio():
    ## SETUP ##
    test_folder = 'test_generate_actual_portfolio'
    assert not os.path.exists(test_folder)
    if not os.path.exists(test_folder):
        os.makedirs(test_folder)    
    slippage = 0.0025
    commissions = 0.0016
    volume_limit = 0.05
    current_date = pandas.datetime.today()
    environment = Environment(test_folder, current_date)
    broker = Broker(environment, slippage, commissions, volume_limit)
    ##
    
    
    ## TEAR DOWN ##
    shutil.rmtree(test_folder) 
    ##