try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

packages = ['avatrader']

setup(
    name="AVATrader",
    version="0.1dev",
    description='Ava\'s trading platform',
    long_description='Ava\'s trading platform',
    keywords='Trading',
    author='Ava',
    maintainer='Ava',
    url='',
    license="MIT License",
    packages=packages,
    install_requires=[
        'numpy',
        'pandas',
        'matplotlib',
        'talib',
        'psycopg2',
        'logging'
    ]
)