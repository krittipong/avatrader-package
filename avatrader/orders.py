from . import *
import os
import numpy
import scipy
import scipy.optimize
import pandas
from decimal import Decimal

class Orders(pandas.DataFrame):

    def __init__(self, environment):
        self.__dict__['_environment']    = environment
        self.__dict__['_orders_columns'] = ['SID', 'SYMBOL', 'ACTION', 'VOLUME', 'LIMITPRICE']
        super().__init__(columns=self._orders_columns)
        self._environment.logger.info("Orders.__init__(): done")


    def from_date(self, trading_date):
        # 1. loading
        assert os.path.exists(trading_date.target_orders)
        super().__init__(pandas.read_csv(
            filepath_or_buffer = trading_date.target_orders, 
            skipinitialspace   = True, 
            index_col          = False))

        assert numpy.array_equal(self.columns, self._orders_columns)
        self.set_index('SID', drop=False, inplace=True)
        
        # 2. logging
        self._environment.logger.debug(
            "\n\nOrders.from_date({:%Y-%m-%d}): self\n{}".format(trading_date(), self))
        self._environment.logger.info(
            "\n\nOrders.from_date({:%Y-%m-%d}): done".format(trading_date()))
            
            
        return self

    def from_portfolios(self, target_portfolio, current_portfolio):
        def _floor_spread(price):
            price = Decimal(str(price))
            if price < 2:
                return float(price - price%Decimal(str(0.01)))
            elif price < 5:
                return float(price - price%Decimal(str(0.02)))
            elif price < 10:
                return float(price - price%Decimal(str(0.05)))
            elif price < 25:
                return float(price - price%Decimal(str(0.10)))
            elif price < 100:
                return float(price - price%Decimal(str(0.25)))
            elif price < 200:
                return float(price - price%Decimal(str(0.50)))
            elif price < 400:
                return float(price - price%Decimal(str(1.0)))
            else:
                return float(price - price%Decimal(str(2.0)))
            
        assert self._environment.current_date()  == target_portfolio.equity['DATE']
        assert self._environment.previous_date() == current_portfolio.equity['DATE']

        # 1. join position tables
        target_portfolio.positions.set_index('SID', drop=False, inplace=True)
        current_portfolio.positions.set_index('SID', drop=False, inplace=True)
        positions_diff = target_portfolio.positions.join(
            other   = current_portfolio.positions,
            how     = 'outer',
            lsuffix = '_TARGET',
            rsuffix = '_CURRENT').fillna(0)

        positions_diff['VOLUME_DIFF'] = positions_diff['VOLUME_TARGET'] - positions_diff['VOLUME_CURRENT']

        self._environment.logger.debug(
            "\n\nOrders.from_portfolios({:%Y-%m-%d}): positions_diff\n{}".format(self._environment.current_date(), positions_diff))
        
        positions_diff = positions_diff[positions_diff['VOLUME_DIFF'] > 0]
        positions_diff['COST_DIFF']   = positions_diff['COST_TARGET'] - positions_diff['COST_CURRENT']

        # 2. calculate sell orders 
        positions_to_sell               = positions_diff[positions_diff['VOLUME_DIFF'] < 0].copy()
        positions_to_sell['SID']        = positions_to_sell['SID_CURRENT']
        positions_to_sell['SYMBOL']     = positions_to_sell['SYMBOL_CURRENT']
        positions_to_sell['ACTION']     = 'SELL'
        positions_to_sell['VOLUME']     = -1 * positions_to_sell['VOLUME_DIFF']
        positions_to_sell['LIMITPRICE'] = 0
        positions_to_sell = positions_to_sell[self._orders_columns]
        
        # 3. calculate buy orders 
        current_cash                   = current_portfolio.equity['CASH']
        positions_to_buy               = positions_diff[positions_diff['VOLUME_DIFF'] > 0].copy()
        positions_to_buy               = self._adjust_cost_and_volume(positions_to_buy, current_cash)
        positions_to_buy               = positions_to_buy[positions_to_buy['VOLUME_DIFF'] > 0]
        positions_to_buy['SID']        = positions_to_buy['SID_TARGET']
        positions_to_buy['SYMBOL']     = positions_to_buy['SYMBOL_TARGET']
        positions_to_buy['ACTION']     = 'BUY'
        positions_to_buy['VOLUME']     = +1 * positions_to_buy['VOLUME_DIFF']
        
#         positions_to_buy = positions_to_buy[(positions_to_buy['COST_DIFF'] > 0)&(positions_to_buy['VOLUME_DIFF'] > 0)]
        
        self._environment.logger.debug(
            "\n\nOrders.from_portfolios({:%Y-%m-%d}): positions_to_buy\n{}".format(self._environment.current_date(), positions_to_buy))
        
        
        positions_to_buy['LIMITPRICE'] = (positions_to_buy['COST_DIFF'] / positions_to_buy['VOLUME_DIFF']).apply(_floor_spread)
        positions_to_buy = positions_to_buy[self._orders_columns]

        assert (positions_to_buy['VOLUME'] * positions_to_buy['LIMITPRICE']).sum() <= current_cash,  ' Not Enough Money !! value to buy  ={}, current_cash = {} '.format( (positions_to_buy['VOLUME'] * positions_to_buy['LIMITPRICE']).sum(), current_cash)

        # 4. reinitialize dataframe
        super().__init__(positions_to_sell.append(positions_to_buy, sort=False))
        self['SID'] = self['SID'].astype(numpy.int64)
        self.set_index('SID', drop=False, inplace=True)
        
        self.drop(self[self['VOLUME']==0].index, inplace=True)
        assert numpy.array_equal(self.columns, self._orders_columns)
            
            
         # 5. logging
        self._environment.logger.debug(
            "\n\nOrders.from_portfolios({:%Y-%m-%d}, {:%Y-%m-%d}): \n{}".format(
                target_portfolio.equity['DATE'],
                current_portfolio.equity['DATE'],
                self))
        self._environment.logger.info(
            "\n\nOrders.from_portfolios({:%Y-%m-%d}, {:%Y-%m-%d}): done".format(
                target_portfolio.equity['DATE'],
                current_portfolio.equity['DATE']))
        return self


    def _adjust_cost_and_volume(self, positions_to_buy, current_cash):
        cost_target  = positions_to_buy['COST_TARGET'].values
        cost_current = positions_to_buy['COST_CURRENT'].values
        limit_price  = positions_to_buy['COST_DIFF'] / positions_to_buy['VOLUME_DIFF']

        self._environment.logger.debug(
            "\n\nOrders._adjust_cost_and_volume({:%Y-%m-%d}): positions_to_buy\n{}".format(self._environment.current_date(), positions_to_buy))
        
        # 1. KL divergence definition
        def KL_divergence(x):
            epsilon = 1e-6
            q = (x + cost_current + epsilon) / numpy.sum(x + cost_current + epsilon)
            p = cost_target / numpy.sum(cost_target)
            kl = -1 * p * numpy.log(q / p)
            return kl.sum()

        # 2. initialize starting point
        x0 = positions_to_buy['COST_DIFF'].values
        x0 = numpy.clip(x0, 0, numpy.inf)
        required_cash = x0.sum()

        # 3. searching for an optimal KL divergence
        if required_cash < current_cash:
            x = numpy.copy(x0)

        else:
            self._environment.logger.warning(
                "\n\nOrders._adjust_cost_and_volume(orders, cash={}): not enough cash to buy".format(current_cash))
            self._environment.logger.warning(
                "\n\nOrders._adjust_cost_and_volume(orders, cash={}): rescale using 'KL_divergence'".format(current_cash))

            bounds = [(0, x0_i) for x0_i in x0]
            constraints = [{'type': 'eq', 'fun': lambda x: (current_cash-1) - x.sum()}]
            x0 = x0 * (current_cash-1) / required_cash
            x = scipy.optimize.minimize(
                fun         = KL_divergence,
                x0          = x0,
                bounds      = bounds,
                constraints = constraints)['x']

        # 4. adjust the positions dataframe
        # positions_to_buy['COST_DIFF']     = x
        # positions_to_buy['COST_TARGET']   = positions_to_buy['COST_CURRENT'] + positions_to_buy['COST_DIFF']
        # positions_to_buy['VOLUME_DIFF']   = x / limit_price
        # positions_to_buy['VOLUME_DIFF']   = numpy.floor(positions_to_buy['VOLUME_DIFF'] / 100) * 100.
        # positions_to_buy['VOLUME_TARGET'] = positions_to_buy['VOLUME_CURRENT'] + positions_to_buy['VOLUME_DIFF']
        
        positions_to_buy['VOLUME_DIFF']   = x / limit_price
        positions_to_buy['VOLUME_DIFF']   = numpy.floor(positions_to_buy['VOLUME_DIFF'] / 100) * 100.
        positions_to_buy['VOLUME_TARGET'] = positions_to_buy['VOLUME_CURRENT'] + positions_to_buy['VOLUME_DIFF']
        positions_to_buy['COST_DIFF']     = positions_to_buy['VOLUME_DIFF'] * limit_price
        positions_to_buy['COST_TARGET']   = positions_to_buy['COST_CURRENT'] + positions_to_buy['COST_DIFF']
            
        self._environment.logger.debug(
            "\n\nOrders._adjust_cost_and_volume(orders, cash={}): done".format(current_cash))
        self._environment.logger.debug(
            "\n\nOrders.positions_to_buy: {} done".format(positions_to_buy))
        
        return positions_to_buy

    def save_as_target_orders(self):
        # 1. validate dataframes
        assert ((self['ACTION'] == 'BUY') | (self['ACTION'] == 'SELL')).all()
        assert (self['VOLUME'] >= 0).all()
        assert (self['LIMITPRICE'] >= 0).all()
        assert numpy.array_equal(self.columns, self._orders_columns)

        # 2. save to files
        assert not os.path.exists(self._environment.current_date.target_orders)
        self.to_csv(self._environment.current_date.target_orders, index=False)
            
        self._environment.logger.info(
            "\n\nOrders.save_as_target_orders(): save to {:%Y-%m-%d}, done".format(self._environment.current_date()))

